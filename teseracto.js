//**************************************************** Setup de eventos a escuchar
require('events').EventEmitter.defaultMaxListeners = 20;
//**************************************************** Node Modules Setup
//-----* Express inicia servidor / carpeta raiz
const express=require('express');
const app=express();

app.use(express.static(__dirname)); 

var https=require('https');
var fss=require('fs');
var key=fss.readFileSync('encryption/server.key');
var cert=fss.readFileSync('encryption/server.cert');
var httpsOptions={key:key,cert:cert};

const server=https.createServer(httpsOptions,app).listen(8888,function(){
	 console.log('Aquiles server activated !');
});

//-----* Socket module object
//var io = require('socket.io')(server); //Bind socket.io to our express server.
const io = require("socket.io")(server, {
  allowEIO3: true // false by default
});

/*/-----* Postgres module object
const  {Client}  = require('pg');
postgresdb = new Client({  // Se cambia a Pool para manejar error de conexion de clientes a prueba para validar funcionamiento.
	//const Client = require('pg')
	//postgresdb = new Client.Pool({
    host: '148.164.96.81',
    user: 'app_aquiles',
    password: 'Redsamuraix1.',
    database: 'aquiles',
});*/

//-----* Filesystem module object
const fs = require('fs');

/*/-----* modbus module object
var ModbusRTU = require("modbus-serial");
var client = new ModbusRTU();
*/
/*	
async function modbusconectionstart(){// open connection to a tcp line
	
	return new Promise(async resolve =>{ 	
	client.connectTCP("172.24.129.251", { port: 502,autoOpen: false },function(){ console.log("\n Conexion establecida");resolve('resolved');});
	client.setID(1);
	});//Cierra Promise
}*/	

//**************************************************** Postgres DB Querys  (DB Conections)

io.on('connection', (socket) => {//Un Socket para todos los requerimientos a postgres

		socket.on('connectdbon', function (){ 
			postgresdb.connect();
			console.log("Postgres conectado");
		}); 

		socket.on('connectdboff', function (){ 
			postgresdb.end();
			console.log("Desconectado");
		}); 
		
		socket.on('getpn', function (np){ 
			get(np);
		}); 
		
		socket.on('setpn', function (np){ 
			set(np);
		}); 
		
		socket.on('setpoints', function (model,targets){ 
			setpnp(model,targets);
		}); 
		
		socket.on('getpoints', function (model){ 
			getp(model);
		}); 
		
		socket.on('getmid', function (model){ 
			getm(model);
		}); 
		
		socket.on('setmipone', function (modelid,targets,sqty,path,camid){ 
			setmidpopa(modelid,targets,sqty,path,camid);
		}); 
		
		socket.on('sampleqty', function (modelid,actualpoint){ 
			getqty(modelid,actualpoint);
		});
		
		socket.on('eraseregister', function (modelid,actualpoint){ 
			erase(modelid,actualpoint);
		});
		
		socket.on('updatesampleqty', function (sqty,modid,acpoint){ 
			update(sqty,modid,acpoint);
		});
		
		socket.on('getcamid', function (modelid,acpoint){ 
			getcameraid(modelid,acpoint);
		});
		
		socket.on('picsaving', async function (datauri,serial,sqty){ // Funcion de ejemplo borrar no importante
			await savingpic(datauri,serial,sqty);
			console.log("recibe",serial,qty);
		});
		
		socket.on('setuptensorfile', async function (nump,actpoint){ // Funcion de ejemplo borrar no importante
			await callfromserver(nump,actpoint);
			console.log("Setting up tensor");
		});
		
		socket.on('tavis', async function (p){ // Funcion de ejemplo borrar no importante
			remote(p);
		});
	
	});//Close io.on

//-----* Get PN Query
async function get(np){  
			await partn(np);
}
	
async function partn(np){ 
	return new Promise(async resolve =>{ 
 
	let partn=np;
	let pg="SELECT model FROM models WHERE model='"+partn+"'";
	 
	postgresdb.query(pg, function (err,result) { 
	
	if (err) { console.log(err.stack);}
		
		else  if (result.rows[0] == undefined) {		
		 
			//console.log("NP NUEVO");
			io.emit('Qrypnresponse', 0);
			resolve('resolved');}
			 
			else {
			io.emit('Qrypnresponse',result.rows[0].model);
			resolve('resolved');
			}//Cierra else	 
		});//Cierra query
	});//Cierra Promise
}	

//-----* Set PN Query
async function set(np){  
			await setpartn(np);
			console.log("Asyncrona: "+np); //console.log("Asyncrona: "+np+"--"+tgt);
}
	
async function setpartn(np){ 
	return new Promise(async resolve =>{ 
 
		 let partn=np;
		 
		 let pg="INSERT INTO models(model) VALUES('"+partn+"');"
		 postgresdb.query(pg, function (err) { 
			 
			 if (err) {//throw err.name;
			 console.log(err.stack);
			 io.emit('setpnresponse', 0);
			 }
			 else{
			 console.log("Numero de parte Insertado pg");
			 io.emit('setpnresponse', 1);
			 }
			
		});//Cierra query
	});//Cierra Promise
}

//-----* Get points Query	
async function getp(model){  
			await getpoints(model);		
}
	
async function getpoints(model){ 
return new Promise(async resolve =>{ 
 
		 let modelo=model;
		 let pg="SELECT model,points FROM models WHERE model='"+modelo+"'";
		  
		postgresdb.query(pg, function (err,result) { 
		
		if (err) {console.log(err.stack);}
			 
			else if (result.rows[0] == undefined) {	
			
				console.log("No points");
				io.emit('Qrympointsresponse', 0);
				resolve('resolved');}
				
				else {					
				console.log("DB respond:",result.rows[0].points);
				io.emit('Qrympointsresponse', result.rows[0].points);
				resolve('resolved');				
				 }
		});//Cierra query
	});//Cierra Promise
}

//-----* Set (Part N) points Query
async function setpnp(model,targets){  
			await uppnandpoints(model,targets);			
}

//-----* Set points
async function uppnandpoints(model,targets){
	return new Promise(async resolve =>{ 
 
		 let partn=model;
		 let points=targets;
		 
		 let pg="INSERT INTO models(model,points) VALUES('"+partn+"',"+points+");"
		postgresdb.query(pg, function (err) { 
			 if (err) {console.log(err.stack);
			 io.emit('setpointsresponse', 0);
			 }
			 else{
			 console.log("Numero de parte Insertado");
			 io.emit('setpointsresponse', 1);
			 }			
		});//Cierra query
	});//Cierra Promise
}

//-----* Get mid Query
async function getm(model){ 
			await getmid(model);
			console.log("Modelmid: "+ model); 
	}
	
async function getmid(model){ 
	return new Promise(async resolve =>{ 
 
		let modelo=model;
		 let pg="SELECT mid,points FROM models WHERE model='"+modelo+"'"; 
		 postgresdb.query(pg, function (err,result) { 
		 console.log("DB respond:",result.rows[0]);
			if (err) {console.log(err.stack);}
			 
			else if (result.rows[0] == undefined) {	
			
			io.emit('Qrymidresponse', 0);
			resolve('resolved');}
			 
			else {
			console.log("DB respond:",result.rows[0].mid);
			io.emit('Qrymidresponse', result.rows[0].mid);
			resolve('resolved');
			}
				
		});//Cierra query
	});//Cierra Promise
}

//-----* (setmipone) Insert mid, point, net file path, Samplesqty, camera id to DB
async function setmidpopa(modelid,targets,sqty,path,camid){  
console.log(camid); 
console.log(path); 
			await setmidpointpath(modelid,targets,sqty,path,camid);			
}

async function setmidpointpath(modelid,targets,sqty,path,camid){

return new Promise(async resolve =>{ 
 
		let mid=modelid;
		let points=targets;
		let samples=sqty;
		let netpath=path;
		let cameraid=camid;
		
		let pg="INSERT INTO neural_nets (mid,point,sampleqty,net,cameraid) VALUES('"+mid+"','"+points+"','"+samples+"','"+netpath+"','"+cameraid+"');"
		postgresdb.query(pg, function (err) { 
		
			 if (err) {console.log(err.stack);
			 console.log("Net data no  insertado");			
			 }
			 else{
			 console.log("Net data Insertado");			 
			 }			
		});//Cierra query
	});//Cierra Promise
}

//-----* Get sample qty
async function getqty(modelid,actualpoint){  
			await getsampleqty(modelid,actualpoint);			
}

async function getsampleqty(modelid,actualpoint){ 
return new Promise(async resolve =>{ 
 
		let mid=modelid;
		let actualp=actualpoint;
		let pg="SELECT sampleqty FROM neural_nets WHERE mid='"+mid+"' and point='"+actualp+"'";
		postgresdb.query(pg, function (err,result) { 
			 
			if (err) {console.log(err.stack);}
			 
			else if (result.rows[0] == undefined) {	
			console.log("No samples found");
			io.emit('sampleqtyresponse', 0);
			resolve('resolved');}
			 
			else {
			console.log("Get sample qty:",result.rows[0].sampleqty);
			io.emit('sampleqtyresponse',result.rows[0].sampleqty);
			resolve('resolved');
			}
			
		});//Cierra query
	});//Cierra Promise
}

//-----* Erase updated register
async function erase(modelid,actualpoint){  
			await eraseregister(modelid,actualpoint);
}

async function eraseregister(modelid,actualpoint){ 
	return new Promise(async resolve =>{ 
 
		let mid=modelid;
		let actualp=actualpoint;
		let pg="DELETE FROM neural_nets WHERE mid='"+mid+"' and point='"+actualp+"'";
			postgresdb.query(pg, function (err,result) { 
			 
			if (err) {console.log(err.stack);}
			 
			else if (result.rowCount == 1) {
				
			console.log("Register erased");
			io.emit('eraseregisterresponse', 1);
			resolve('resolved');}
			
			else {
			console.log("No register to erase");
				io.emit('eraseregisterresponse', 0);
				}
		});//Cierra query
	});//Cierra Promise
}

//-----* Update sample qty
async function update(sqty,modid,acpoint){  
	await updatesampleqty(sqty,modid,acpoint);		
}

async function updatesampleqty(sqty,modid,acpoint){
	return new Promise(async resolve =>{ 
 
		let newsampleqty=sqty;
		let modelid=modid;
		let actualpoint=acpoint
		let pg="UPDATE neural_nets SET sampleqty = '"+newsampleqty+"' WHERE (mid = '"+modelid+"' AND point = '"+actualpoint+"' );";
		console.log(result); 
		postgresdb.query(pg, function (err,result) { 
			 if (err) {console.log(err.stack);}
			 
			 else if (result.rows[0] == undefined) {	
			console.log("Update succesful");			
			resolve('resolved');}
			
			 else {
			 console.log("Update not succesful");			
			 resolve('resolved');
			}		
		});//Cierra query
	});//Cierra Promise
}

//-----* Get cemera ID
async function getcameraid(modelid,actualpoint){  
			await idcam(modelid,actualpoint);			
}

async function idcam(modelid,actualpoint){ 
return new Promise(async resolve =>{ 
 
		let mid=modelid;
		let actualp=actualpoint;
		let pg="SELECT cameraid FROM neural_nets WHERE mid='"+mid+"' and point='"+actualp+"'";
		
		postgresdb.query(pg, function (err,result) { 
			 
			if (err) {console.log(err.stack);}
			 
			else if (result.rows[0] == undefined) {	
			console.log("Camera ID not found");
			io.emit('getcamidresponse', 0);
			resolve('resolved');}
			 
			else {
			console.log("Cam ID:",result.rows[0].cameraid);
			io.emit('getcamidresponse',result.rows[0].cameraid);
			resolve('resolved');
			}
			
		});//Cierra query
	});//Cierra Promise
}

//-----* Load Neural Network data

function callfromserver(nump,actpoint){
	
	
	let np=nump;
	let actualpoint=actpoint;	
	let data;
	let file_path;	
    file_path= "samples/"+np+"_net_point_"+actualpoint+"_neural_network.json";
	
	const fs = require('fs');    
	let rawdata = fs.readFileSync(file_path);	
	data = JSON.parse(rawdata);
	
	io.emit('loadtensor', data);
	console.log("NET JSON Enviado");

}


//**************************************************** Files Handler


//-----* Crea archivo y guarda tensor de neural network
io.on('connection', (socket) => {
		socket.on('nettofile', function (json,pn,point){ 
			
			let jfile=json;
			let partn=pn;
			let net_point=point;
			var fs = require('fs');
			let netpath= 'samples/'+pn+'_net'+'_point_'+net_point+'_neural_network.json';			
			fs.writeFile('samples/'+pn+'_net'+'_point_'+net_point+'_neural_network.json',jfile, function (err) {
			if (err) throw err;
			console.log('Neural net JSON saved!');});
			io.emit('netpath', netpath);
		});	
	});

//-----* Borra archivo

io.on('connection', (socket) => {
		socket.on('deletefile', function (path){ 
			deletef(path);
		}); //Close socket
		
		
		socket.on('logsaving', async function (sn,logsaving){ // Funcion de ejemplo borrar no importante
			savinglog(sn,logsaving)
		});
		socket.on('renombrasnr', function (snr){ // conexion con main_script
			renombraF(snr);
		});

	});	
	
async function deletef(path){  
			await deletefile(path);		
}

async function deletefile(path){

	return new Promise(async resolve =>{ 
 
		netpath=path;
		fs.unlinkSync(netpath)
		console.log("Archivo borrado: "+ netpath); 
		
	});//Cierra Promise
}

//-----* Guarda imagen desde URI
async function savingpic(datauri,serial,sqty){
	
	let filePath;
	const ImageDataURI = require('image-data-uri');
	return new Promise(async resolve =>{ 	
	//console.log("Variables:"+serial+' - '+sqty+'');// temporal para ver que esta rebiendo 
	
	//C:/Users/mayra_ayala/Documents/Aquiles/img/
	//C:/Users/gdl3_mds/myapp/timsamples/
	let filePath='C:/Users/gdl3_mds/myapp/timsamples/'+serial+'';//Ruta de las carpetas por serial
	let filevalidation=fs.existsSync(filePath);
	
	if (filevalidation){ 

		filePath=''+filePath+'/'+sqty+'';		
		ImageDataURI.outputFile(datauri, filePath).then(res => console.log(res));	
	}
	else{		
		fs.mkdir(filePath,(error)=>{		
			if (error){
				console.log(error.message);//en caso de que el folder ya exista manda un error y evita hacer otro folder con el mismo nombre.
				}
				filePath=''+filePath+'/'+sqty+'';		
				ImageDataURI.outputFile(datauri, filePath).then(res => console.log(res));
				console.log("Directorio creado");
			});
		}
	});//Cierra Promise
}

// Pendiente crear funcion para renombrar carpeta F 

async function renombraF(serial){
		fs.rename('C:/Users/gdl3_mds/myapp/timsamples/'+serial,
		'C:/Users/gdl3_mds/myapp/timsamples/'+serial+'_F',	
		 function (err) {
 			   if (err) 
  				console.log('Error de renombramiento');
		});
}

function savinglog(sn,logdata){

	let  logpath= 'C:/Users/gdl3_mds/myapp/timsamples/'+sn+'/log.txt';	
		fs.writeFile(logpath,logdata, function (err) {
		if (err) throw err;});
}
//**************************************************** Protocolo de comunicacion

//*----- Modbus - Serial
io.on('connection', (socket) => {
		socket.on('movehw', function (hwp,modbus_ip,modbus_port,modbus_xpointreg,modbus_inpositionreg){
			movehw(hwp,modbus_ip,modbus_port,modbus_xpointreg,modbus_inpositionreg);
			console.log("Move HW Socket");
		}); 
		socket.on('pass', function(serialnumber,pjct,unitnumber) {//Un Socket para todos los requerimientos a  42Q
			pass(serialnumber,pjct,unitnumber); 
		});
		socket.on('fail', function(serialnumber,pjct,unitnumber) {//Un Socket para todos los requerimientos a  42Q
			fail(serialnumber,pjct,unitnumber); 
		});	
		socket.on('workstation', function(serialnumber,pjct,unitnumber){//Un Socket para todos los requerimientos a 42Q
			workstation(serialnumber,pjct,unitnumber); 
		});
		
});

let reg_in_position_flag=0,counter=0;

//-----* Move HW to point x
async function movehw(hwp,modbus_ip,modbus_port,modbus_xpointreg,modbus_inpositionreg){
	//console.log("Arguments :",hwp,modbus_ip,modbus_port,modbus_xpointreg,modbus_inpositionreg)
	
	reg_in_position_flag=0,counter=0;
	await conecta(modbus_ip,modbus_port);
	
	await x_position(hwp);
	await pause();
	await regloop(modbus_inpositionreg);//Espera HW hasta que este en posicion 
	
	io.emit('inposition', 1);
	console.log("Termino movimiento");
	
	//closeconnection();
	
}
async function pause(){
	return new Promise(async resolve =>{
		setTimeout(function pausea(){resolve('resolved')},500);				
				});	
}
async function x_position(position,modbus_xpointreg){
	
	return new Promise(async resolve =>{
			let pos=position;
			client.writeRegister(modbus_xpointreg, pos,function(err, data) {//Position status register
			console.log("Position: "+ pos);
			console.log(data);});
			resolve('resolved');	
		//setTimeout(function wait_con(){console.log("Xposition");resolve('resolved')},3000);
			});//Cierra Promise
	
}
async function regloop(modbus_inpositionreg){
		
		while(reg_in_position_flag==0 && counter <60){
			await conecta();
			counter=counter+1;		
			await lee_reg(modbus_inpositionreg);
			await register_logic();	
			//closeconnection();
		
		}//Cierra while
}
async function conecta(modbus_ip,modbus_port){
			return new Promise(async resolve =>{
				//console.log("Conecta :",modbus_ip,modbus_port)
				client.connectTCP(modbus_ip, { port: modbus_port,autoOpen: false },function(){ 
				console.log("\nConexion establecida");
				//resolve('resolved')	
				setTimeout(function wait_con(){resolve('resolved')},500);				
				});	
	
			});//Cierra Promise
		}		
async function lee_reg(modbus_inpositionreg){
	return new Promise(async resolve =>{
			client.setID(1);	
			client.readInputRegisters(modbus_inpositionreg, 1,function(err,data) {		
				reg_in_position_flag=data.data;
				console.log("Valor del registro: "+reg_in_position_flag);
				resolve('resolved');
			});//Cierra lectura de registro
	});//Cierra Promise
}
async function register_logic(){
	
		return new Promise(async resolve =>{		
			
			if (reg_in_position_flag!=0){console.log("Flag diferente de 0 : "+reg_in_position_flag);resolve('resolved');}
			if (reg_in_position_flag==0){console.log("Flag igual a 0");resolve('resolved');}	
			
			
		});//Cierra Promise 
	}//Cierra lee_reg
async function closeconnection(){
	console.log("Conexion terminada");
	client.close();
}

//*----- 42Q
var http = require('http');
function pass(serialnumber,pjct,unitnumber) {
		
	
// Build the post string from an object
let sn = serialnumber
let projectname=pjct
let unit=unitnumber
console.log(sn);
var post_data = {
		"serial": ""+sn+"",
		"test_id": "Inspection",
        "station": "Tape_Inspection",
        "operator": "999999",
        "password": "",
        "type": "PRODUCTION",
        "process_name": "Tape_Inspection",
        "status": "PASSED"
		
	};

	var  myJSON = JSON.stringify(post_data);
    console.log(" Asi es como manda la peticion al servidor: "+myJSON+"\n");
	
     // An object of options to indicate where to post to
     var post_options = {
		 host: 'production.42-q.com',
        // port: '8080',
		path: '/mes-api/p2447dc1c/measurement/async/transaction/27c7ad42-fb2c-4693-bf5b-3fd562c4d645/ProcessRequest',
         method: 'POST',
		 headers: {'Content-type':'application/json'}
     };
	 
	// Set up the request object
     var post_req = http.request(post_options, function(res) {
         res.on('data', function (chunk) {
         console.log('Response: ' + chunk);
         });
     });

     // post the data
     post_req.write(myJSON);
     post_req.end();

}

function fail(serialnumber,pjct,unitnumber) {
    // Build the post string from an object
let sn = serialnumber;
let projectname=pjct
let unit=unitnumber  
//console.log(sn);
  var post_data = {
	"serial": ""+sn+"",//
	"test_id": "Inspection",
	"station": "Tape_Inspection",
	"operator": "999999",
	"password": "",
	"type": "PRODUCTION",
	"process_name": "Tape_Inspection",
	"status": "FAILED"
	};

	var  myJSON = JSON.stringify(post_data);
    console.log(" Asi es como manda la peticion al servidor: "+myJSON+"\n");
	
     // An object of options to indicate where to post to
     var post_options = {
		host: 'production.42-q.com',
        // port: '8080',
         path: '/mes-api/p2447dc1c/measurement/async/transaction/27c7ad42-fb2c-4693-bf5b-3fd562c4d645/ProcessRequest',
         method: 'POST',
		 headers: {'Content-type':'application/json'}
     };
	 
	// Set up the request object
     var post_req = http.request(post_options, function(res) {
         res.on('data', function (chunk) {
         console.log('Response: ' + chunk);
         });
     });

     // post the data
     post_req.write(myJSON);
     post_req.end();

}

function workstation(serialnumber,pjct,unitnumber) {
	
	let sn = serialnumber;
	let projectname = pjct
	let unit = unitnumber  
	console.log(sn);
// Build the post string from an object
	let res;
     var post_data = {
		"projectname" : ""+projectname+"",
		"unit":""+unit+"",
        "employee_number": "TST99999",
        "password" : "",
         
	};

	 var  myJSON = JSON.stringify(post_data);
    //console.log(" Asi es como manda la peticion al servidor: "+myJSON+"\n");
	
     // An object of options to indicate where to post to 
     var post_options = {
         host: '143.116.205.46',
         port: '8080',

		 //--------------------------------------Sfdcpc---/numero de serie de la unidad
         path: '/Aquiles_Back/infoSerial/summary/p2448dc5/'+sn+'',
         method: 'GET',
		 headers: {'Content-type':'application/json'}
     };
	 
	let data='';
	let json;
	// Set up the request object
     var post_req = http.request(post_options, function(chunk) {		
			// Collect all chunks in data
			chunk.on('data', function (response) {			
			data += response;
			});			
		});		
	 setTimeout(function espera(){io.emit('workstationresponse',data)},1000);
     // post the data
     post_req.write(myJSON);
     post_req.end();

}



//*----- TCP/IP PLC TESLA
let plc_endresponse=0
io.on('connection', (socket) => {

	socket.on('plc_response', function(result_matrix){
		plcdatasender(result_matrix) 
	});
	
});

var net = require('net');

var tcpipserver = net.createServer(function(connection) { 
   console.log('TCP client connected');
   //connection.on('end', function() {
      //console.log(Buffer.toString());  
   //});
   connection.write('Handshake ok!\r\n');
   //connection.pipe(connection);

   //Funcion para imprimir la cadena que le envianconsole.log(data)
   //connection.on('data', function(data) { console.log(data.toString())});
   connection.on('data', function(data) { io.emit('Timsequence_start',data.toString());console.log("Analisis in process...");})

  //console.log('Received bytes: ' + data);
  
  //Inicia la secuencia
   
  //Responde a PLC cuando termine inspeccion
  setTimeout(function respuesta(){
	  estadoconexion = connection.readyState
	  console.log("Comunicacion con el plc :"+connection.readyState)
	  
		 if (estadoconexion == 'closed' ){
			 console.log("Puerto de PLC cerrado reintento en 1min..."  )
		 }
		 if (estadoconexion == 'open'){
				connection.write(plc_endresponse)
		 }

	},40000) // tiempo de secuencia completa

	})

function plcdatasender(result_matrix) {
	matrixtostring=result_matrix.toString()
	plc_endresponse=matrixtostring
}


tcpipserver.listen(40000, function() { 
   console.log('PLC Port 40000 listening...');
});
/*

function remote(p){
	
var net = require('net');

.+
	p =''+p+'';
var client = net.connect(PORT,HOST,function() { //'connect' listener
  console.log('Tavis on line, value '+p+' sent../ '+typeof(p)+' - Type');  
  client.write(p);
  setTimeout(function findesesion(){client.end();},500);
  });

client.on('data', function(data) {
  console.log(data.toString());
  if (data == "SIDES"){
	  client.write('2');
  }
  client.end();
});


}*/




